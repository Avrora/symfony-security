<?php

// 1. Dans le base.html.twig, au dessus du block body, rajouter un a qui aura comme href le path vers la route logout
// 2. Toujours dans le base.html.twig, faire un if/else pour faire que si on a un app.user, 
// alors on affiche le logout, et sinon on affiche un autre lien qui mène vers le login
// 3. Créer une nouvelle entité Tech qui aura juste une propriété label de type string et 
// une relation ManyToMany avec l'entité User (nullable des deux côté)
// 4. Faire un ptit make:migration et un migrations:migrate
// 5. Créer le formulaire du User (et en profiter pour faire la méthode __toString de l'entité Tech)
// 6. Dans le HomeController (qui commence à être trop plein à mon gout), 
// faire une Route register dans laquelle vous mettrez le formulaire et son traitement qui fait persister le user 
// (ça sera notre route d'inscription)
// Pour encrypter le mot de passe:
// * Dans les argument de la méthode, en rajouter un typé en UserPasswordEncoderInterface
// * A l'intérieur du if du formulaire, utiliser la méthode encodePassword de cet argument en lui donnant en premier argument l'objet $user et en deuxième argument $user->getPassword(), stocker ça dans une variable
// * Faire un $user->setPassword() en lui donnant en argument la variable faite à l'étape d'avant
// * Faire le persist et le flush comme d'hab
// 7. Dans le else du base.html.twig, à côté du lien vers la page login, rajouter un lien vers la page register
// 8. Créer le formulaire de l'entité Tech puis aller le modifier pour retirer le user des champs du formulaire (s'il y est)
// 9. Créer un nouveau controller TechController avec une route "/profile/add-tech" 
// qui contiendra le formulaire d'ajout de tech, la fera persister etc.

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\User;
use App\Form\UserType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\Persistence\ObjectManager;



class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('home/index.html.twig', [
            'controller_name' => 'HomeController',
        ]);
    }

    /**
    * @Route("/profile", name="profile")
    */
    public function profile(){
        return $this->render("home/profile.html.twig", [

        ]);
    }

    /**
    * @Route("/login", name="login")
    */
    public function login(AuthenticationUtils $auth){
        $errors = $auth->getLastAuthenticationError();
        $username = $auth->getLastUsername();

        return $this->render("home/login.html.twig", [
            "errors" => $errors,
            "username" => $username
        ]);
    }

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request,
                            UserPasswordEncoderInterface $encoder, 
                            ObjectManager $manager) {

    $user = new User();
    $form = $this->createForm(UserType::class, $user);

    $form->handleRequest($request);

    if($form->isSubmitted() && $form->isValid()) {
        $pass = $encoder->encodePassword($user, $user->getPassword());
        $user->setPassword($pass);
            
        $manager->persist($user);
        $manager->flush();
        
        // return $this->redirectToRoute('login');
    }

    return $this->render('home/register.html.twig', [
        'form' => $form->createView()    
        
    ]);
    }

}
