<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Tech;
use App\Form\TechType;

class TechController extends AbstractController
{
    /**
     * @Route("/profile/add-tech", name="tech")
     */
    public function index(Request $request, ObjectManager $manager)
    {
        $tech = new Tech();
        $form = $this->createForm(TechType::class, $tech);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $manager->persist($tech);
            $manager->flush();
        }

        return $this->render('tech/index.html.twig', [
            'form' => $form->createView()
        ]);
    }
}

