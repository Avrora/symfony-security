<?php

namespace App\Example;

class FirstImplementation implements FirstInterface {
    public function doStuff(int $param): string
    {
        return "bloup" . $param;
    }

   
}

// public function methode(FirstInterface $impl) {
//     echo $impl->doStuff(1);
// }