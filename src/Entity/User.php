<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Tech", inversedBy="techs")
     */
    private $techs;

    public function __construct()
    {
        $this->techs = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getUsername() {
        return $this->email;
    }

    public function getRoles() {
        return ["ROLE_USER"];
    }

    public function getSalt() {}

    public function eraseCredentials() {}

    /**
     * @return Collection|Tech[]
     */
    public function getTechs(): Collection
    {
        return $this->techs;
    }

    public function addTech(Tech $tech): self
    {
        if (!$this->techs->contains($tech)) {
            $this->techs[] = $tech;
            $tech->addUser($this);
        }

        return $this;
    }

    public function removeTech(Tech $tech): self
    {
        if ($this->techs->contains($tech)) {
            $this->techs->removeElement($tech);
            $tech->removeUser($this);
        }

        return $this;
    }

}
